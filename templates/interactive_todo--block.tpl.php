<?php

/**
 * @file
 * Contains template information of to do list.
 */
$add_item = l(t('Add task'), 'interactive_todo/nojs/add_note', array('attributes' => array('id' => array('add-item'), 'class' => array('use-ajax'))));
$clear_all = l(t('Clear all tasks'), 'interactive_todo/nojs/clear_all', array('attributes' => array('id' => array('clear-all'), 'class' => array('use-ajax'))));
$pending_count = count($data['pending']);
$completed_count = count($data['completed']);
?>

<div id="interactive_todo_container" class='todo_wrapper'>
    <header class="dash-blocks dash-header">
        <div class='todo_wrapper_count card-count'><?php print '<b>' . t('Total tasks') . '</b>' . '(' . ($pending_count + $completed_count) . ')'; ?></div>
    </header>
    <div class="dash-blocks dash-body">
        <div class='todo_wrapper_tabs'>
            <ul class="tab-nav row">
                <?php foreach ($data['tabs'] as $key => $value): ?>

                        <?php
                        if ($value == t("Pending")):
                            print "<li class='active col'><a id='show_pending' href='javascript:void(0)' onclick='change_view(this)'>" . $value . " " . $pending_count . "</a></li>";
                        endif;
                        if ($value == t("Completed")):
                            print "<li class='col'><a id='show_completed' href='javascript:void(0);' onclick='change_view(this)'>" . $value . " " . $completed_count . "</a></li>";
                        endif;
                        ?>

                <?php endforeach; ?>
            </ul>
        </div>

        <div id='todo_content_pending'>
            <ul class="dash-content-list">
                <?php foreach ($data['pending'] as $key => $value): ?>
                    <li>
                        <span id="todo_pending_item_<?php print $value->id; ?>" class="todo_pending_title" ><?php print strip_tags($value->note); ?></span>
                        <?php
                        print l(
                        t('Mark as done'),
                        'interactive_todo/nojs/mark_as_done',
                        array(
                          'query' => array('item' => $value->id),
                          'attributes' => array(
                            'title' => 'Mark as done',
                            'id' => array('mark-as-done'),
                            'class' => array('use-ajax status-btn pending'),
                          ),
                        )
                        );

                        print "<i>" . date('F j, Y, g:i a', $value->changed) . "</i>";
                        ?>
                    </li>
                <?php endforeach; ?>
                <?php
                if ($pending_count == 0) {
                  print "<span>" . t("No tasks available.") . "</span>";
                }
                ?>
            </ul>
        </div>

        <div id='todo_content_completed' style="display:none;">
            <ul class="dash-content-list">
                <?php foreach ($data['completed'] as $key => $value): ?>
                    <li>
                        <span id="todo_completed_item_<?php print $value->id; ?>" class="todo_completed_title"><strike><?php print strip_tags($value->note); ?></strike></span>
                        <?php

                        print l(
                        t('Delete'),
                         'interactive_todo/nojs/delete',
                          array(
                            'query' => array('item' => $value->id),
                            'attributes' =>
                            array(
                              'title' => 'Delete',
                              'id' => array('delete'),
                              'class' => array('use-ajax status-btn completed'),
                            ),
                          )
                        );

                        print l(
                        t('Undo'),
                         'interactive_todo/nojs/revert',
                          array(
                            'query' => array('item' => $value->id),
                            'attributes' =>
                            array(
                              'title' => 'Revert',
                              'id' => array('revert'),
                              'class' => array('use-ajax status-btn undo'),
                            ),
                          )
                        );

                        print "<i>" . date('F j, Y, g:i a', $value->changed) . "</i>";
                        ?>
                    </li>
                <?php endforeach; ?>
                <?php
                if ($completed_count == 0) {
                  print "<span>" . t("No tasks available.") . "</span>";
                }
                ?>
            </ul>
        </div></div>

    <footer class="dash-blocks dash-footer">
        <ul class="context-actions">
            <li class='add_note'><i class="fa fa-plus"></i><?php print $add_item; ?>

            <div id="operation_container" class="context-form"></div>
            </li>
            <li class='delete_notes'><i class="fa fa-trash-o"></i><?php print $clear_all; ?></li>
        </ul>
    </footer>
</div>

<script type="text/javascript">
  function change_view(whichthis) {
    if (whichthis.id === "show_pending") {
      jQuery('#todo_content_pending').show();
      jQuery('#todo_content_completed').hide();
      jQuery('#show_pending').parent('li').addClass('active');
      jQuery('#show_completed').parent('li').removeClass('active');
    }
    if (whichthis.id === "show_completed") {
      jQuery('#todo_content_pending').hide();
      jQuery('#todo_content_completed').show();
      jQuery('#show_pending').parent('li').removeClass('active');
      jQuery('#show_completed').parent('li').addClass('active');
    }
  }
</script>
