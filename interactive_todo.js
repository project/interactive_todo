/**
 * @file
 * Managing display of note content.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.interactive_todo = {
    attach: function (context, settings) {
      if (typeof Drupal.ajax !== 'undefined') {
        if (typeof Drupal.ajax['interactive_todo_submit_note_content'] !== 'undefined') {
          Drupal.ajax['interactive_todo_submit_note_content'].beforeSerialize = function () {
            $('#interactive_todo_cancel_note_content').hide();
          };
        }
      }
    }
  };
})(jQuery);
