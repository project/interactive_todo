<?php

/**
 * @file
 * Contains forms and action items.
 */

/**
 * Callback from interactive todo actions items.
 */
function _interactive_todo_action($js, $some_argument) {
  if ($js) {
    switch ($some_argument) {
      case 'add_note':
        $content = drupal_get_form('_interactive_todo_add_note_form');
        break;

      case 'clear_all':
        $content = drupal_get_form('_interactive_todo_clear_all_note_form');
        break;

      case 'mark_as_done':
        $content = _interactive_todo_mark_as_done();
        $commands[] = ajax_command_html('#interactive_todo_container', $content);
        $content = '';
        break;

      case 'delete':
        $content = _interactive_todo_delete_todo_item();
        $commands[] = ajax_command_html('#interactive_todo_container', $content);
        $commands[] = ajax_command_invoke('#todo_content_pending', 'hide');
        $commands[] = ajax_command_invoke('#todo_content_completed', 'show');
        $content = '';
        break;

      case 'revert':
        $content = _interactive_todo_undo_todo_item();
        $commands[] = ajax_command_html('#interactive_todo_container', $content);
        $commands[] = ajax_command_invoke('#todo_content_pending', 'hide');
        $commands[] = ajax_command_invoke('#todo_content_completed', 'show');
        $content = '';
        break;
    }
    $commands[] = ajax_command_html('#operation_container', render($content));
    $commands[] = ajax_command_invoke('#operation_container', 'show');
    print ajax_render($commands);
    drupal_exit();
  }
  else {
    $output = t("No content available.");
    return $output;
  }
}

/**
 * Form for adding note.
 */
function _interactive_todo_add_note_form() {
  $form = array();
  $form['status_msg'] = array(
    '#markup' => "<div id='_interactive_todo_status_msg'></div>",
  );
  $form['note_area'] = array(
    '#type' => 'textfield',
    '#title' => t('Add a news item'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#id' => 'interactive_todo_submit_note_content',
    '#ajax' => array(
      'callback' => 'interactive_todo_submit_note_content',
    ),
    '#attributes' => array('class' => array('add-list')),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#id' => 'interactive_todo_cancel_note_content',
    '#ajax' => array(
      'callback' => 'interactive_todo_cancel_note_content',
    ),
    '#attributes' => array('class' => array('cancel-list')),
  );
  return $form;
}

/**
 * Submit handler for add note form.
 */
function interactive_todo_submit_note_content($form, &$form_state) {
  global $user;
  $note = trim(check_plain($form_state['values']['note_area']));
  if (empty($note)) {
    $result = array(
      '#type' => 'ajax',
      '#commands' => array(
        ajax_command_invoke('#interactive_todo_cancel_note_content', 'show'),
        ajax_command_html('#_interactive_todo_status_msg', t('Note data required.')),
      ),
    );
  }
  else {
    $fields = array(
      'uid' => $user->uid,
      'note' => $note,
      'created' => time(),
      'changed' => time(),
    );
    db_insert('interactive_todo_info')->fields($fields)->execute();
    $content = _interactive_todo_content();
    $result = array(
      '#type' => 'ajax',
      '#commands' => array(
        ajax_command_html('#interactive_todo_container', $content),
        ajax_command_html('#operation_container', t('Data saved successfully.')),
        ajax_command_invoke('#operation_container', 'fadeOut',
        array(
          1500,
          'linear',
          'complete',
        )
        ),
      ),
    );
  }
  return $result;
}

/**
 * Callback for cancel note.
 */
function interactive_todo_cancel_note_content($form, &$form_state) {
  $result = array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_html('#operation_container', ''),
    ),
  );
  return $result;
}

/**
 * Form for clear all notes.
 */
function _interactive_todo_clear_all_note_form() {
  $form = array();
  $form['status_msg'] = array(
    '#markup' => t("<label>Do you wish to clear all notes ?</label>"),
    '#attributes' => array('class' => array('add-list')),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
    '#ajax' => array(
      'callback' => 'interactive_todo_clear_all_items_confirm',
    ),
    '#attributes' => array('class' => array('add-list')),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('No'),
    '#ajax' => array(
      'callback' => 'interactive_todo_clear_all_items_cancel',
    ),
    '#attributes' => array('class' => array('add-list')),

  );
  return $form;
}

/**
 * Clears all the notes callback function.
 */
function interactive_todo_clear_all_items_confirm($form, &$form_state) {
  global $user;
  db_delete('interactive_todo_info')->condition('uid', $user->uid)->execute();
  $content = _interactive_todo_content();
  $result = array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_html('#operation_container', ''),
      ajax_command_html('#interactive_todo_container', $content),
    ),
  );
  return $result;
}

/**
 * Cancels the clear operation of todo items.
 */
function interactive_todo_clear_all_items_cancel($form, &$form_state) {
  $result = array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_html('#operation_container', ''),
    ),
  );
  return $result;
}

/**
 * Mark as done.
 */
function _interactive_todo_mark_as_done() {
  global $user;
  $item = $_REQUEST['item'];
  $fields = array(
    'status' => 1,
    'changed' => time(),
  );
  db_update('interactive_todo_info')
    ->fields($fields)
    ->condition('uid', $user->uid)
    ->condition('id', $item)
    ->execute();
  $content = _interactive_todo_content();
  return $content;
}

/**
 * Delete the todo item.
 */
function _interactive_todo_delete_todo_item() {
  global $user;
  $item = $_REQUEST['item'];
  db_delete('interactive_todo_info')->condition('id', $item)->condition('uid', $user->uid)->execute();
  $content = _interactive_todo_content();
  return $content;
}

/**
 * Undo the todo item.
 */
function _interactive_todo_undo_todo_item() {
  global $user;
  $item = $_REQUEST['item'];
  $fields = array(
    'status' => 0,
    'changed' => time(),
  );
  db_update('interactive_todo_info')
    ->fields($fields)
    ->condition('uid', $user->uid)
    ->condition('id', $item)
    ->execute();
  $content = _interactive_todo_content();
  return $content;
}
